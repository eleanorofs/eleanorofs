## Highlights 

### Chicago Test Out

In early 2020, both the Sun-Times and the Tribune were exclusively tracking state-level data, and the City of Chicago hadn't yet published any data visualization tools, so over Memorial Day weekend, I banged out the first version of [Chicago Test Out](https://chicagotestout.gitlab.io/) (so named because, at the time, only testing data was available). 

Over time, the city did release some data visualization tools, but since they take several minutes to load and don't work on mobile, I continued to iterate on CTO and continued to use it to help me manage risk for myself and my loved ones. 

### ReScript/JavaScript Bindings

ReScript, as a new functional language, doesn't come with a set of bindings to the browser APIs. I've taken to formalizing the bindings I need with standard conventions. They're useful to me as a developer, and they're a great way to learn about ReScript. They're also a lot of fun to write in their own right because it's a pure way of playing with type theory without thinking about implementation details. I've enjoyed writing them, and I hope you enjoy using them. 

* [`rescript-push`](https://www.npmjs.com/package/rescript-push) - for the Web Push API
* [`rescript-notifications`](https://www.npmjs.com/package/rescript-notifications) - for the Notifications API
* [`rescript-service-worker`](https://www.npmjs.com/package/rescript-service-worker) - for the Service Worker API
* [`rescript-indexeddb`](https://www.npmjs.com/package/rescript-indexeddb) - for the IndexedDB API. 
* [`res-elm`](https://www.npmjs.com/package/res-elm) - for interoperation between the Elm functional programming language and ReScript through Elm ports.

### [Tutorials](https://webbureaucrat.gitlab.io/)

Although I generally don't write about my professional work, I've enjoyed using "webbureaucrat" as a light-hearted moniker referring to my happy career in public service. 

A few of my favorite articles are: 

* [Hello, Idris World! and Why I'm Excited for a Total Programming 
Language](https://webbureaucrat.gitlab.io/articles/hello-idris-world/) - My 
first tutorial when I was playing around with the Idris programming language.
* [Safer Data Parsing with Try Monads](https://webbureaucrat.gitlab.io/articles/safer-data-parsing-with-try-monads/) - in which I advocate for an *end* to uncaught exceptions in object-oriented languages
* [Elm Line Charts](https://webbureaucrat.gitlab.io/articles/elm-line-charts-part-i-times-and-timezones/) - a series which documents how to create time-based data visualizations like those in [Chicago Test Out](https://chicagotestout.gitlab.io/)
* [Binding to a JavaScript Function that Returns a Variant in ReScript](https://webbureaucrat.gitlab.io/articles/binding-to-a-javascript-function-that-returns-a-variant-in-rescript/) - because maintaining perfect, static type safety in a language as dynamic as JavaScript is nuanced and fun and fascinating. 
* [Parsing JSON in ReScript](https://webbureaucrat.gitlab.io/articles/parsing-json-in-rescript-part-i-prerequisites-and-requirements/) - because functional programmers just love writing parsers and the more parsers we write, the happier we are. 
